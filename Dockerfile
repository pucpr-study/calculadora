# Use Node.js as base image
FROM node:18

# Crie um diretório de trabalho
WORKDIR /app

# Copie todos os arquivos da aplicação para o diretório de trabalho
COPY . .

# Instale o módulo http-server globalmente
RUN npm install http-server -g

# Exponha a porta 8080
EXPOSE 8080

# Defina o comando para iniciar o http-server
CMD ["sh", "-c", "http-server -p 8080 --open /app/calculadora.html | tee -a /app/log/access.log"]

# Defina a pasta de trabalho padrão para o diretório de trabalho
WORKDIR /app


