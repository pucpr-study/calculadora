const display = document.querySelector('.display');
const keys = document.querySelectorAll('.keys button');

// Seleciona o botão "C" na calculadora
const resetButton = document.querySelector("#reset");

keys.forEach(key => {
  key.addEventListener('click', event => {
    const keyContent = event.target.textContent;
    const displayValue = display.textContent;

    if (keyContent === 'C') {
      display.textContent = '0';
    } else if (keyContent === '=') {
      const expression = displayValue.replace(/x/g, '*').replace(/÷/g, '/');
      display.textContent = eval(expression);
    } else if (displayValue === '0') {
      display.textContent = keyContent;
    } else {
      display.textContent = displayValue + keyContent;
    }
  });
});

// Adiciona um evento de clique no botão "C"
resetButton.addEventListener("click", () => {
  calculator.clear();
  calculator.updateDisplay();
});

